package simple_factory;

import beer.Beer;

public class Main {

    public static void main(String[] args) {
        BeerFactory beerFactory = new BeerFactory();
        Beer myBeer = beerFactory.createBeer("Blackbeer");

        System.out.println(myBeer.getName());
    }
}
