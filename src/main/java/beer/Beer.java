package beer;

/**
 * The beer classes are some kind of "helper classes" for
 * the design patterns. They contain some content, to demonstrate
 * the useage of the design patterns.
 *
 * @author Tom Dittrich duke64@gmx.de
 * @version 0.1
 */
public interface Beer {
    
    /**
     * The name of the beer.
     * 
     * @return name
     */
    public String getName();
}
